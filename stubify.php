<?php

session_start();

define('NAME_EXPRESSION', '/^(class|interface|trait|function|constant)_([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)$/');

$map =
[
	'class' =>
	[
		'label' => 'Classes',
		'source' => 'get_declared_classes',
		'generator' => 'generateClassStub',
		'postfix' => ''
	],
	'interface' =>
	[
		'label' => 'Interfaces',
		'source' => 'get_declared_interfaces',
		'generator' => 'generateClassStub',
		'postfix' => ''
	],
	'trait' =>
	[
		'label' => 'Traits',
		'source' => 'get_declared_traits',
		'generator' => 'generateTraitStub',
		'postfix' => ''
	],
	'function' =>
	[
		'label' => 'Functions (by prefix)',
		'source' => 'getFunctionPrefixes',
		'generator' => 'generateFunctionStubs',
		'postfix' => 'functions'
	],
	'constant' =>
	[
		'label' => 'Constants (by prefix)',
		'source' => 'getConstantPrefixes',
		'generator' => 'generateConstantStubs',
		'postfix' => 'constants'
	]
];

if (array_key_exists('download', $_GET))
{
	$matches = [];
	if (preg_match(NAME_EXPRESSION, $_GET['download'], $matches) === false)
	{
		http_send_status(400);
		echo '<h1>Bad request</h1>';
		die;
	}

	$type = $matches[1];
	$name = $matches[2];

	$filePostfix = $map[$type]['postfix'];
	header("Content-Type: text/plain");
	header("Content-Disposition: attachment; filename=$name$filePostfix.php");
	header("Pragma: no-cache");
	echo $_SESSION['results'][$type][$name];
	exit;
}

if ($_POST)
{
	$_SESSION['results'] = [];

	foreach ($_POST['targets'] as $target)
	{
		$matches = [];
		if (preg_match(NAME_EXPRESSION, $target, $matches) === false)
		{
			continue;
		}

		$type = $matches[1];
		$name = $matches[2];

		$_SESSION['results'][$type][$name] = call_user_func($map[$type]['generator'], $name);
	}
}

function generateClassStub($className)
{
	$outputBuffer = '<?php' . PHP_EOL . PHP_EOL;
	$reflectionClass = new ReflectionClass($className);

	if($reflectionClass->inNamespace())
	{
		$outputBuffer .= 'namespace ' . substr($reflectionClass->getNamespaceName(), 1) . ';' . PHP_EOL . PHP_EOL;
	}

	$outputBuffer .= $reflectionClass->isAbstract() && !$reflectionClass->isInterface() ? 'abstract ' : '';
	$outputBuffer .= $reflectionClass->isFinal() ? 'final ' : '';
	$outputBuffer .= $reflectionClass->isInterface() ? 'interface ' : 'class ';
	$outputBuffer .= "$className" . PHP_EOL;
	$outputBuffer .= "{" . PHP_EOL;

	$outputBuffer .= generateConstants($reflectionClass);
	$outputBuffer .= generateProperties($reflectionClass);
	$outputBuffer .= generateMethods($reflectionClass);

	$outputBuffer .= "}" . PHP_EOL;

	return $outputBuffer;
}

function generateTraitStub($traitName)
{
	// TODO
	return '';
}

function generateFunctionStubs($functionPrefix)
{
	$result = get_defined_functions();
	$functions = getItemsByPrefix(array_merge($result['internal'], $result['user']), $functionPrefix);

	$outputBuffer = '<?php' . PHP_EOL . PHP_EOL;

	foreach($functions as $function)
	{
		$reflectionFunction = new ReflectionFunction($function);

		if ($reflectionFunction->getDocComment())
		{
			$outputBuffer .= cleanDocComment($reflectionFunction->getDocComment(), 0) . PHP_EOL;
		}
		else
		{
			$outputBuffer .= '/**' . PHP_EOL;
			$outputBuffer .= ' * @return mixed' . PHP_EOL;
			$outputBuffer .= ' */' . PHP_EOL;
		}

		$outputBuffer .= 'function ';
		$outputBuffer .= $reflectionFunction->returnsReference() ? '&' : '';
		$outputBuffer .= $reflectionFunction->name;
		$outputBuffer .= '(';

		foreach ($reflectionFunction->getParameters() as $parameter)
		{
			$outputBuffer .= $parameter->getClass() ? $parameter->getClass()->getName() . ' ' : '';
			$outputBuffer .= $parameter->isPassedByReference() ? '&' : '';
			$outputBuffer .= '$' . $parameter->getName();
			if ($parameter->isDefaultValueAvailable())
			{
				$outputBuffer .= ' = ';
				if ($parameter->isDefaultValueConstant())
				{
					$outputBuffer .= $parameter->getDefaultValueConstantName();
				}
				else
				{
					$outputBuffer .= formatValue($parameter->getDefaultValue());
				}
			}
			$outputBuffer .= ', ';
		}

		if (substr($outputBuffer, -2) == ', ')
		{
			$outputBuffer = substr($outputBuffer, 0, -2);
		}
		$outputBuffer .= ')';
		$outputBuffer .= PHP_EOL . '{' . PHP_EOL . '}' . PHP_EOL . PHP_EOL;
	}

	return $outputBuffer;
}

function generateConstantStubs($constantPrefix)
{
	$outputBuffer = '<?php' . PHP_EOL . PHP_EOL;

	foreach(get_defined_constants() as $name => $value)
	{
		if (strpos($name, $constantPrefix) === 0)
		{
			$value = formatValue($value);
			$outputBuffer .= "define('$name', $value);" . PHP_EOL;
		}
	}

	return $outputBuffer;
}

function generateConstants(ReflectionClass $reflectionClass, $indentLevel = 1)
{
	$outputBuffer = '';
	$indent = str_repeat("\t", $indentLevel);

	foreach($reflectionClass->getConstants() as $name => $value)
	{
		$value = formatValue($value);
		$outputBuffer .= $indent . "const $name = $value;" . PHP_EOL . PHP_EOL;
	}

	return $outputBuffer;
}

function formatValue($value)
{
	if (is_string($value))
	{
		$value = "\"$value\"";
	}

	if (is_bool($value))
	{
		$value = strval($value);
	}

	return $value;
}

function cleanDocComment($docComment, $indentLevel)
{
	$indent = str_repeat("\t", $indentLevel);
	$docCommentLines = explode("\n", $docComment);
	for ($i = 0; $i < count($docCommentLines); $i++)
	{
		$docCommentLines[$i] = ltrim($docCommentLines[$i]);
		if ($docCommentLines[$i][0] === '*')
		{
			$docCommentLines[$i] = ' ' . $docCommentLines[$i];
		}
		$docCommentLines[$i] = $indent . $docCommentLines[$i];
	}
	return implode("\n", $docCommentLines);
}

function generateProperties(ReflectionClass $reflectionClass, $indentLevel = 1)
{
	$outputBuffer = '';
	$indent = str_repeat("\t", $indentLevel);
	foreach ($reflectionClass->getProperties() as $reflectionProperty)
	{
		if ($reflectionProperty->getDocComment())
		{
			$outputBuffer .= cleanDocComment($reflectionProperty->getDocComment(), $indentLevel) . PHP_EOL;
		}
		else
		{
			$outputBuffer .= $indent . '/**' . PHP_EOL;
			$outputBuffer .= $indent . ' * @property mixed $' . $reflectionProperty->name . PHP_EOL;
			$outputBuffer .= $indent . ' */' . PHP_EOL;
		}
		$outputBuffer .= $indent;
		$outputBuffer .= $reflectionProperty->isPublic() ? 'public ' : '';
		$outputBuffer .= $reflectionProperty->isProtected() ? 'protected ' : '';
		$outputBuffer .= $reflectionProperty->isPrivate() ? 'private ' : '';
		$outputBuffer .= $reflectionProperty->isStatic() ? 'static ' : '';
		$outputBuffer .= "\$$reflectionProperty->name;" . PHP_EOL . PHP_EOL;
	}

	return $outputBuffer;
}

function generateMethods(ReflectionClass $reflectionClass, $indentLevel = 1)
{
	$outputBuffer = '';
	$indent = str_repeat("\t", $indentLevel);
	foreach ($reflectionClass->getMethods() as $reflectionMethod)
	{
		if ($reflectionMethod->getDocComment())
		{
			$outputBuffer .= cleanDocComment($reflectionMethod->getDocComment(), $indentLevel) . PHP_EOL;
		}
		else
		{
			$outputBuffer .= $indent . '/**' . PHP_EOL;
			$outputBuffer .= $indent . ' * @return mixed' . PHP_EOL;
			$outputBuffer .= $indent . ' */' . PHP_EOL;
		}

		$outputBuffer .= $indent;
		$outputBuffer .= $reflectionMethod->isPublic() ? 'public ' : '';
		$outputBuffer .= $reflectionMethod->isProtected() ? 'protected ' : '';
		$outputBuffer .= $reflectionMethod->isPrivate() ? 'private ' : '';
		$outputBuffer .= $reflectionMethod->isStatic() ? 'static ' : '';
		$outputBuffer .= $reflectionMethod->isFinal() ? 'final ' : '';
		$outputBuffer .= 'function ';
		$outputBuffer .= $reflectionMethod->returnsReference() ? '&' : '';
		$outputBuffer .= $reflectionMethod->name;
		$outputBuffer .= '(';

		foreach ($reflectionMethod->getParameters() as $parameter)
		{
			$outputBuffer .= $parameter->getClass() ? $parameter->getClass()->getName() . ' ' : '';
			$outputBuffer .= $parameter->isPassedByReference() ? '&' : '';
			$outputBuffer .= '$' . $parameter->getName();
			if ($parameter->isDefaultValueAvailable())
			{
				$outputBuffer .= ' = ';
				if ($parameter->isDefaultValueConstant())
				{
					$outputBuffer .= $parameter->getDefaultValueConstantName();
				}
				else
				{
					$outputBuffer .= formatValue($parameter->getDefaultValue());
				}
			}
			$outputBuffer .= ', ';
		}

		if (substr($outputBuffer, -2) == ', ')
		{
			$outputBuffer = substr($outputBuffer, 0, -2);
		}
		$outputBuffer .= ')';

		if($reflectionClass->isInterface() || $reflectionMethod->isAbstract())
		{
			$outputBuffer .= ';' . PHP_EOL . PHP_EOL;
		}
		else
		{
			$outputBuffer .= PHP_EOL . $indent . '{' . PHP_EOL . $indent . '}' . PHP_EOL . PHP_EOL;
		}
	}

	if (substr($outputBuffer, -(strlen(PHP_EOL) * 2)) == PHP_EOL . PHP_EOL)
	{
		$outputBuffer = substr($outputBuffer, 0, -(strlen(PHP_EOL)));
	}

	return $outputBuffer;
}

function getFunctionPrefixes()
{
	$result = get_defined_functions();
	return getPrefixes(array_merge($result['internal'], $result['user']));
}

function getConstantPrefixes()
{
	return getPrefixes(array_keys(get_defined_constants()));
}

function getPrefixes($array)
{
	$prefixes = [];
	foreach ($array as $item)
	{
		if (($offset = strpos($item, '_', 1)) !== false)
		{
			$prefix = substr($item, 0, $offset + 1);
			if (count(getItemsByPrefix($array, $prefix)) > 1 && !in_array($prefix, $prefixes))
			{
				$prefixes[] = $prefix;
			}
		}
	}
	return $prefixes;
}

function getItemsByPrefix($array, $prefix)
{
	$items = [];
	array_walk($array, function($item) use ($prefix, &$items) {
		if (strpos($item, $prefix) === 0)
		{
			$items[] = $item;
		}
	});
	return $items;
}

?>
<html>
<head>
	<meta charset="US-ASCII">
	<style type="text/css">
		#targets
		{
			margin: 1rem 0;
			width: 20rem;
			height: 40rem;
		}

		.output
		{
			margin: 1rem 0;
			width: 50rem;
			height: 20rem;
			overflow: auto;
			white-space: nowrap;
		}

		form
		{
			margin: 2rem;
		}
	</style>
</head>
<body>
	<?php if (!$_POST): ?>
		<form method="POST">
			<div>
				<label for="targets">Targets:</label>
			</div>
			<div>
				<select id="targets" name="targets[]" multiple="multiple">
					<?php foreach($map as $type => $properties): ?>
					<optgroup label="<?=$properties['label']?>">
						<?php $targets = call_user_func($properties['source']); ?>
						<?php sort($targets, SORT_FLAG_CASE); ?>
						<?php foreach($targets as $name): ?>
						<option value="<?=$type?>_<?=$name?>"><?=$name?></option>
						<?php endforeach; ?>
					</optgroup>
					<?php endforeach; ?>
				</select>
			</div>
			<div>
				<input type="submit" value="Generate" />
			</div>
		</form>
	<?php else: ?>
		<?php foreach($map as $type => $properties): ?>
			<?php if (!isset($_SESSION['results'][$type])) continue; ?>
			<h1><?=$properties['label']?></h1>
			<?php foreach($_SESSION['results'][$type] as $name => $stub): ?>
				<div>
					<label for="<?=$type?>_<?=$name?>"><?=$name?><?=$properties['postfix']?></label>
					<a href="?download=<?=$type?>_<?=$name?>">(download)</a>
				</div>
				<div>
					<pre><textarea class="output" id="<?=$type?>_<?=$name?>"><?=$stub?></textarea></pre>
				</div>
			<?php endforeach; ?>
		<?php endforeach; ?>
	<?php endif; ?>
</body>
</html>