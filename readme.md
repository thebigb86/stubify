#Stubify

Stubify is a simple stub generator for internal PHP functions classes, specifically targeting those introduced by
third-party extensions, that don't provide stubs themselves.

#Why?

As mentioned, some third-party extensions don't provide stubs, and stubs are very useful when you use a decent IDE like 
[PhpStorm](https://www.jetbrains.com/phpstorm/). By placing the stubs somewhere in your source tree, the IDE will 
provide you with intelligent auto-completion.

#How does it work?

Easy.

1. Place the `stubify.php` file in an web-accessible directory of the server that loads all the extensions you need.

2. Go to `[your-web-root]/stubify.php` in your browser and select any items and prefixes you would like to have stubs 
for.

3. Click the `Generate` button.

4. ???

5. Profit. You now should have a page with text-areas containing the generated stubs. Click the download link next 
to the item names to save the stubs as files. 

Command line invocation coming soon.

#How does it work internally?

It uses the PHP reflection API to detect class members, function parameters and doc-blocks. If the extension provides 
doc-blocks in its stubs, those doc-blocks will be used for the generated stubs (but in that case they probably already 
provide stubs). Otherwise, a generic doc-block is built. There's not much more to it.

#Todo

- Implement stub generator for traits

- Download all button?

- Some kind of caching for the lists?

- Allow usage from CLI

- Code cleanup

- Perhaps some simple styling